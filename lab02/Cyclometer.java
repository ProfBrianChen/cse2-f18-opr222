//Owen Rahr 9/6/18 CSE002
//Lab 2: Cyclometer

public class Cyclometer{
  //Main method for every java program
  public static void main(String[] args){
    //Variables:
    int secsTrip1 = 480; //Time elapsed in seconds of the first trip
    int secsTrip2 = 3220; //Time elapsed in seconds of the second trip
    int countsTrip1 = 1561; //Number of tire rotations in the first trip
    int countsTrip2 = 9037; //Number of tire rotations in the second trip
    double wheelDiameter = 27.0; //Tire diameter
    double PI = 3.14159;  //Necessary measurement
    int feetPerMile = 5280;  //Necessary measurement
  	int inchesPerFoot = 12;   //Necessary measurement
  	int secondsPerMinute = 60;  //Necessary measurement
	  double distanceTrip1, distanceTrip2,totalDistance;  //Outputs
    
    //These next lines print the time and counts for each trip
    System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes and had " + countsTrip1 + " counts.");
	  System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and had " + countsTrip2 + " counts.");
    //These calculate the distance of each trip in inches by multiplying the circumference of the tire by the number of rotations
    distanceTrip1 = countsTrip1 * PI * wheelDiameter;
    distanceTrip2 = countsTrip2 * PI * wheelDiameter;
    //Now the distance in inches will be converted to miles
    distanceTrip1 /= (inchesPerFoot * feetPerMile);
    distanceTrip2 /= (inchesPerFoot * feetPerMile);
    totalDistance = distanceTrip1 + distanceTrip2; //Calculates total distance
    //These last lines print the calculated values
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
    System.out.println("Trip 2 was "+distanceTrip2+" miles");
    System.out.println("The total distance was "+totalDistance+" miles");
    
  }//end of main method
}//end of class