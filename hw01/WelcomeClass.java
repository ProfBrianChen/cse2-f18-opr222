//Owen Rahr, 9/3/2018. Cse002
//hw01: WelcomeClass

public class WelcomeClass{
  
  public static void main(String args[]){
    
    System.out.println("  ----------- ");
    System.out.println("  | WELCOME | ");
    System.out.println("  ----------- ");
    System.out.println("  ^  ^  ^  ^  ^  ^ ");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\ ");
    System.out.println("<-O--P--R--2--2--2-> ");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ / ");
    System.out.println("  v  v  v  v  v  v ");
    
  }
      
}