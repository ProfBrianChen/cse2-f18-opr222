//Owen Rahr 9/13/18 CSE002
//Lab 03: Check

import java.util.Scanner; //Imports a scanner to detect user inputs

public class Check{
  public static void main(String[] args) { //Sets up method for the program
    
    Scanner myScanner = new Scanner( System.in ); //Declares a scanner, allowing for inputs to be recorded
    
    System.out.print("Enter the original cost of the check in the form xx.xx: "); 
    //Prompts user for cost of check
    double checkCost = myScanner.nextDouble(); //Declares a variable, and sets its value to the user's input
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): " );
    //Prompts user for tip percentage
    double tipPercent = myScanner.nextDouble(); //Declares a double for tip percent, set as the user's next input
    tipPercent /= 100; //Turns the percentage into a decimal
    System.out.print("Enter the number of people who went out to dinner: ");
    //Prompts user for the number of people
    int numPeople = myScanner.nextInt(); //Declares an integer for number of people, set as the user's next input
   
    double totalCost; //Cost including tip
    double costPerPerson; //Cost per person
    int dollars, dimes, pennies; //An int for the dollars, dimes, and pennies each.
    
    totalCost = checkCost * (1 + tipPercent); //Calculates total cost
    costPerPerson = totalCost / numPeople; //Calculates the cost per person
    dollars=(int)costPerPerson; //Takes the number of dollars each person pays by explicitly casting as an integer
    dimes=(int)(costPerPerson * 10) % 10; //Calculates dimes by removing pennies place, then finding the remainder after dividing by ten
    pennies=(int)(costPerPerson * 100) % 10; //Calculates pennies the same way, but after multiplying by 100
   
    System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies);
    //Outputs the final result

    
  }
}