//Owen Rahr 10/7/2018 CSE002
//hw05: While Loops
//This program randomly generates poker hands as many times as prompted, and calculates the probabibilty for each hand

import java.util.Scanner; //Imports a scanner

public class HandProbability {
  public static void main(String[] args) { //Main method fo the program
    
    Scanner scan = new Scanner(System.in); //Declares a new scanner
    int onePairs = 0; //Declares an integer that counts the number of one pairs
    int twoPairs = 0; //Declares an integer that counts the number of two pairs
    int threeOfAKinds = 0; //Declares an integer that counts the number of three of a kinds
    int fourOfAKinds = 0; //Declares an integer that counts the number of four of a kinds
    int tempDupeCounter = 0; //Declares an integer that temporarily counts the number of duplicates
    int tempPairCounter = 0; //Declares an integer that temporarily counts pairs
    
    System.out.print("Enter how many hands to generate: "); //Prompts user
    boolean correct = scan.hasNextInt(); //Creates and assigns a boolean value depending on if an integer was inputted
    
    while (!correct) { //If the input wasn't an int, the body of the loop will run
      System.out.print("Incorrect type, enter again: "); //error message
      scan.next(); //Clears the value from the conveyor belt
      correct = scan.hasNextInt(); //Records the user's new input and assigns a boolean value based on type
    }
    int counter = scan.nextInt(); //Uses the integer from before and assigns it to the variable counter
    
    /*This for loop runs as many times as specified by the user's input. Each time the loop runs, it generates
      a hand of cards, checking for whether a card has already been drawn. The numerical value is then calculated,
      and then tested for if it is one of the types being recorded. If a hand is one of the types being recorded,
      then it adds one to that hand type's counter.*/
    
    int i = 0; //Declares and initializes an integer to be a counter
    while (i < counter) { //Sets up loop to repeat as many times as the counter variable says
        int card1 = (int)(Math.random()*52 + 1); //Assigns variable card1 with a random card
        
        int card2 = (int)(Math.random()*52 + 1); //Does the same as line above
        while (card2 == card1) {                 //If card2's value is the same as card1's, it will generate a new one
          card2 = (int)(Math.random()*52 + 1);
        }
        
        int card3 = (int)(Math.random()*52 + 1); //Same process for card2, checking for all previous card values
        while (card3 == card1 || card3 == card2) {
          card3 = (int)(Math.random()*52 + 1);
        }
        
        int card4 = (int)(Math.random()*52 + 1); //Same as before
        while (card4 == card1 || card4 == card2 || card4 == card3) {
          card4 = (int)(Math.random()*52 + 1);
        }
        
        int card5 = (int)(Math.random()*52 + 1); //Same as before
        while (card5 == card1 || card5 == card2 || card5 == card3 || card5 == card4) {
          card5 = (int)(Math.random()*52 + 1);
        }
        
        card1 %= 14; //This converts the card's identity 1-52 to its number value 1-13
        card2 %= 14; //The suit of the card isn't necessary, so it does not need to be calculated
        card3 %= 14;
        card4 %= 14;
        card5 %= 14;
        //System.out.println(card1 + " " + card2 + " " + card3 + " " + card4 + " " + card5);
        //Above is a check that I used to test this part of the program
      int j = 1; 
        /*This for loop runs thirteen times, once for each card value. If a card's value is the same as the 
          current loop's number, it increments the variable tempDupeCounter*/
        while (j <= 13) {
        if (j == card1) {
          tempDupeCounter++;
          }
        if (j == card2) {
          tempDupeCounter++;
          }
        if (j == card3) {
          tempDupeCounter++;
          }
        if (j == card4) {
          tempDupeCounter++;
          }
        if (j == card5) {
          tempDupeCounter++;
          }
        
        /*This switch tests if each value had more than one duplicate of a card. If it did, it increments the
          counter for the type of hand that it is.*/
        switch (tempDupeCounter) {
          case 2:
            tempPairCounter++; //Since a hand can have more than one pair, this keeps track of the pairs for the hand
            break;
          case 3:
            threeOfAKinds++;
            break;
          case 4:
            fourOfAKinds++;
            break;
        } //End of switch
        ++j; //Increments j, so that it can run a new numbr through
        tempDupeCounter = 0; //Resets the temporary duplicate counter
        } //End of for loop(1-13)
        if (tempPairCounter == 1) { //After every card value is checked for, this checks if there were two pairs
          onePairs++; //If there was only one two pair, it increments the one pair counter
        }
        if (tempPairCounter == 2) { //After every card value is checked for, this checks if there were two pairs
          twoPairs++; //The counter for the two pair counter is incremented if there were two
        }
        tempPairCounter = 0; //This resets the temporary pair counter for next hand
        ++i; //Increments the counter for the loop
    } //End of while loop(0-counter)
  
    double probOne = (double)onePairs / counter; //Calculates the probability as a double for each result
    double probTwo = (double)twoPairs / counter; 
    double probThree = (double)threeOfAKinds / counter; 
    double probFour = (double)fourOfAKinds / counter; 
    
    System.out.println("The number of loops: " + counter); //Prints the number of loops done
    System.out.printf("The probability of Four-of-a-kind: %.4f %n", probFour); //Prints and formats result for a 4 of a kind
    System.out.printf("The probability of Three-of-a-kind: %.4f %n", probThree); //Same as above
    System.out.printf("The probability of Two-pair: %.4f %n", probTwo);
    System.out.printf("The probability of One-pair: %.4f %n", probOne);
    
  }
}
  