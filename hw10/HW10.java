//Owen Rahr CSE002 12/4/2018
//HW10: Tic Tac Toe
//The program plays tic tac toe. Also, I changed the starting player to X, because that is regulation

import java.util.Scanner; //imports scanner


public class HW10 {
  
  public static void main(String[] args) { //Start of main method
    Scanner scan = new Scanner(System.in); //Declare scanner
    String again = ""; //String to record response
    do {
      newGame(); //Runs method that plays the whole game
      System.out.println("Input 'again' if you want to play again.");
      again = scan.next(); //Records response
    } while (again.equals("again")); //If the correct response is given the loop repeats
  } //End of main method
  
  
  public static void newGame() { //Start of method that plays the whole game
    String[][] board = {{"1", "2", "3"}, {"4", "5", "6"}, {"7", "8", "9"}}; //Initializes 2D array for the board
    Scanner scan = new Scanner(System.in); //Declares scanner
    boolean gameOver = false; //Boolean that records if the game is over
    String currPlayer = "X"; //String that keeps track of X and O
    String currTile = ""; //String that records what tile to place on
    boolean validEntry = false; //Boolean that keeps track of if the tile is valid
    int[] location = new int[2]; //Array to keep track of where to swap a tile
    while (!gameOver) { //Loop continues until the game is over
      printBoard(board); //Method that prints the board
      validEntry = false; //Resets the check for the proper tile
      System.out.println("'" + currPlayer + "'" + " player input what tile to place on.");
      do { //Loop that makes sure you enter a valid tile
        currTile = scan.next(); //Scans user input
        for (int row = 0; row < 3; row++) { //The two loops cycle through the whole 2D array
          for (int column = 0; column < 3; column++) {
            if (currTile.equals(board[row][column])) { //If the entry is open, the check is passed, and the location recorded
              validEntry = true;
              location[0] = row;
              location[1] = column;
            }
          }
        }
        if (!validEntry) { //If the entry is not available or isn't valid, it prints an error message
          printBoard(board);
          System.out.println("Invalid entry, enter again");
        }
      } while (!validEntry); //The check if there is a proper response
      board[location[0]][location[1]] = currPlayer; //Replaces the tile with the current player's token
      gameOver = checkGameOver(board, currPlayer); //Passes the board and the current player to a method to check endGame
      if (currPlayer.equals("O")) { //The rest just changes players
        currPlayer = "X";
      }
      else {
        currPlayer = "O";
      }
    } //End of game loop
  } //End of newGame
  
  public static void printBoard(String[][] printedBoard) { //Method goes through the board and prints it out
    for (int row = 0; row < 3; row++) { //Two loops satisfy both dimensions of the array
      for (int column = 0; column < 3; column++) {
        System.out.print(printedBoard[row][column] + "   "); //Prints out the board with space for clarity
      }
      System.out.println(); //After a row, it skips a line to the next one
    }
    return;
  } //End of printBoard
  
  public static boolean checkGameOver(String[][] check, String currPlayer) { //Method checks if game is over
    boolean gameOver = false; //A boolean that can be switched by a number of conditions
    //Every following if statement is a win condition. I could have made it all in one if statement but that would look gross
    if (check[0][0] == check[0][1] && check[0][1] == check[0][2]) {
      gameOver = true;
      System.out.println("'" + currPlayer + "'" + " wins!");
    }
    if (check[1][0] == check[1][1] && check[1][1] == check[1][2]) {
      gameOver = true;
      System.out.println("'" + currPlayer + "'" + " wins!");
    }
    if (check[2][0] == check[2][1] && check[2][1] == check[2][2]) {
      gameOver = true;
      System.out.println("'" + currPlayer + "'" + " wins!");
    }
    if (check[0][0] == check[1][0] && check[1][0] == check[2][0]) {
      gameOver = true;
      System.out.println("'" + currPlayer + "'" + " wins!");
    }
    if (check[0][1] == check[1][1] && check[1][1] == check[2][1]) {
      gameOver = true;
      System.out.println("'" + currPlayer + "'" + " wins!");
    }
    if (check[0][2] == check[1][2] && check[1][2] == check[2][2]) {
      gameOver = true;
      System.out.println("'" + currPlayer + "'" + " wins!");
    }
    if (check[0][0] == check[1][1] && check[1][1] == check[2][2]) {
      gameOver = true;
      System.out.println("'" + currPlayer + "'" + " wins!");
    }
    if (check[0][2] == check[1][1] && check[1][1] == check[2][0]) {
      gameOver = true;
      System.out.println("'" + currPlayer + "'" + " wins!");
    }
    
    boolean noSpaces = true; //Boolean that checks if there are any more available tiles
    if (!gameOver) { //Only enters if none of the other conditions were met
      for (int row = 0; row < 3; row++) {
        for (int column = 0; column < 3; column++) {
          if (!(check[row][column].equals("X") || check[row][column].equals("O"))) { //If any spot isn't an X or O, there's an open space
            noSpaces = false;
          }
        }
      }
    if (noSpaces) { //If there weren't any open spaces, it's a draw, and game over
      System.out.println("Draw!");
      gameOver = true;
    }
    }
    return gameOver; //Returns the response, true or false
  } //End of checkGameOver

} //End of class
      