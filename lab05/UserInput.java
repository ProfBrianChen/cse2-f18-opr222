//Owen Rahr 10/4/2018 CSE002
//Lab05: UserInput
//This code uses loops to check whether or not a user has inputted the correct type of statement, like an int or a string
//It will prompt for a class's info and print it back out.

import java.util.Scanner; //Imports a scanner

public class UserInput {
  public static void main(String[] args){ //Main method for the program
    
    Scanner myScanner = new Scanner(System.in); //Declares a scanner to detect user input
    
    System.out.print("Print the course number: "); //Prompts user for the course number
    boolean correct = myScanner.hasNextInt(); //Scans the user's input and assigns a boolean value depending on whether or not it was an integer
    
    /*Each while loop uses a boolean value as gotten in line 14, if the input isn't an int(the boolean would be false), then 
    the interior of the while loop is run. In the while loop, an error message is printed and a new value is recorded and assigned
    a boolean value based on its type. When the boolean is true, the while loop is exited and the value recorded is assigned to
    its respective variable. This process is repeated for each value recorded(slightly different for strings).*/
    while(!correct) {
      System.out.println("Incorrect type, enter again");
      myScanner.next(); //Clears the conveyor belt, so a new value can be recorded
      correct = myScanner.hasNextInt(); //Records a variable and assigns a boolean value, based on its type
    }
    int courseNum = myScanner.nextInt(); //Assigns the value from the conveyor belt to a newly declared int
   
    
    System.out.print("Print the department name: "); //Same process as lines 13-25, but with a twist to check for a string
    correct = myScanner.hasNextDouble(); //Since there isn't a way to check specifically for a string, a double checks for everything that isn't a string
    
     while(correct) { //Instead of checking for if it isn't correct, it checks if it is a double. A string will evaluate to false, exiting the loop
      System.out.println("Incorrect type, enter again");
      myScanner.next();
      correct = myScanner.hasNextDouble();
    }
    String department = myScanner.next();
    
   
    System.out.print("Print the number of times the course meets in a week: "); //Same process for checking for an int (lines 13-25)
    correct = myScanner.hasNextInt();
    
    while(!correct) {
      System.out.println("Incorrect type, enter again");
      myScanner.next();
      correct = myScanner.hasNextInt();
    }
    int numMeetings = myScanner.nextInt();
    
    
    System.out.print("Print the time of day the course begins in military time(no semicolon or spaces): "); //Same process for an int
    correct = myScanner.hasNextInt();
    
    while(!correct) {
      System.out.println("Incorrect type, enter again");
      myScanner.next();
      correct = myScanner.hasNextInt();
    }
    int timeOfDay = myScanner.nextInt();
    
    
    System.out.print("Print the instructor's last name: "); //Checks for a string(lines 28-36)
    correct = myScanner.hasNextDouble();
    
    while(correct) {
      System.out.println("Incorrect type, enter again");
      myScanner.next();
      correct = myScanner.hasNextDouble();
    }
    String instructorName = myScanner.next();
    
    
    System.out.print("Print the number of students in the class: "); //Checks for int
    correct = myScanner.hasNextInt();
    
    while(!correct) {
      System.out.println("Incorrect type, enter again");
      myScanner.next();
      correct = myScanner.hasNextInt();
    }
    int numStudents = myScanner.nextInt();
    
    
    int timeMinutes = timeOfDay % 100; //Takes the last two digits of the time of day, or minutes
    int timeHours = timeOfDay / 100; //Takes the first two digits of time, or the hours. These two lines make it clearer to display time using a semicolon.
    
    System.out.println("Your course is " + department + " " + courseNum + " taught by Professor " + instructorName);
    System.out.println("It meets " + numMeetings + " times per week at " + timeHours + ":" + timeMinutes + ". There are " + numStudents + " students in the class.");
    //Lines 86-87 display all of the values recorded.
    
  }
}