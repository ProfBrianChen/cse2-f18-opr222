//Owen Rahr 9/20/18
//lab04: Card Generator
//This is a program that generates a random card, suit and value

public class CardGenerator{
  public static void main(String[] args){ //Main method for the program
    
    int suitNum = (int)(Math.random()*4 + 1); //Declares an int and gives it a random value between 1 and 4 for each suit
    String suitString; //Declares a string to be replaced with the suit type
  switch (suitNum) { //Turns the random suit number into the name of a suit
      case 1:  suitString = "Hearts";
        break;
      case 2:  suitString = "Diamonds";
        break;
      case 3:  suitString = "Clubs";
        break;
      case 4:  suitString = "Spades";
        break;
      default:  suitString = "Invalid";
    }
    int cardNum = (int)(Math.random()*13 + 1); //Declares an int and assigns it a random value between 1 and 13 for each card identity
    String cardString; //Declares a string to be replaced with the card's identity
  switch (cardNum) { //Turns the random card number into the corresponding name of the card
      case 1:  cardString = "Ace";
        break;
      case 2:  cardString = "2";
        break;
      case 3:  cardString = "3";
        break;
      case 4:  cardString = "4";
        break;
      case 5:  cardString = "5";
        break;
      case 6:  cardString = "6";
        break;
      case 7:  cardString = "7";
        break;
      case 8:  cardString = "8";
        break;
      case 9:  cardString = "9";
        break;
      case 10:  cardString = "10";
        break;
      case 11:  cardString = "Jack";
        break;
      case 12:  cardString = "Queen";
        break;
      case 13:  cardString = "King";
        break;
      default:  cardString = "Invalid";
    }
    
    System.out.println("You picked the " + cardString + " of " + suitString);
    //Prints the final result using each string
    
  }
}