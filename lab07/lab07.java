//Owen Rahr 10/25/2018 CSE002
//Lab 007: Random sentence generator
//This program randomly genereates sentences using methods

import java.util.Scanner; //imports scanner class
import java.util.Random; //imports random class

public class lab07 { //Start of the public class
  public static void main(String[] args) { //Start of main method
    boolean check; //initializes a boolean to check user input
    do { //Loop that outputs a sentence until user inputs a number
      //Next line prints a sentence by calling different methods to input random words
      System.out.println("The " + adjective() + " " + adjective() + " " + subject() + " " + verb() + " the " + adjective() + " " + object());
      System.out.println("If you would like another sentence, enter a number. Otherwise, enter anything else"); //prompt
      Scanner scan = new Scanner(System.in); //Initializes a scanner
      check = scan.hasNextDouble(); //Sets check to true if the input is a double
    } while (check); //If check is false, it exits the loop. if true, it continues
    thesis(); //Calls thesis method
  }
  
  public static void thesis() { //Method that prints out a thesis for a small paragraph
     String mainCharacter = subject(); //Initializes a string as a random subject, that will be referenced several times
     //Next line generates another sentence about the mainCharacter
     System.out.println("The " + adjective() + " " + adjective() + " " + mainCharacter + " " + verb() + " the " + adjective() + " " + object());
     bodySentences(mainCharacter); //Passes mainCharacter to bodySentences method
  }
  
  public static void bodySentences(String a) { //This method prints two more sentences about the main character
    Random randomGeneratorSt = new Random(); //Declare a scanner
    int randomStart = randomGeneratorSt.nextInt(2); //Initializes an int as a random number to change up the starts of sentences
    //The if else statements pick one of two sentence starters, then print a sentence about the main character
    //Each sentence will have a different sentence starter
    if (randomStart == 1) {
      System.out.println("It was " + adjective() + " to " + adjective() + " " + object() + "s");
      System.out.println("The " + a + " " + verb() + " a " + adjective() + " " + object());
    }
    else {
      System.out.println("The " + a + " was " + adjective() + " to " + adjective() + " " + object() + "s");
      System.out.println("It " + verb() + " a " + adjective() + " " + object());
    }
    conclusion(a); //Passes the main character to the conclusion method
  }
  
  public static void conclusion(String a) { //Final sentence of the paragraph
    System.out.println("That " + a + " " + verb() + " his or her " + object() + "s!"); //Prints sentence
  }
 
  public static String adjective() { //Each of the rest of these classes generates a random type of word
    Random randomGeneratorA = new Random(); //Declares a scanner
    int randomNumAdj = randomGeneratorA.nextInt(10); //Initializes an int as a random number 1-10
    String randomAdj; //Declares a string
    switch (randomNumAdj) { //Each case sets the string to a different word
      case 1: randomAdj = "fluffy";
      break;
      case 2: randomAdj = "slimy";
      break;
      case 3: randomAdj = "jittery";
      break;
      case 4: randomAdj = "clammy";
      break;
      case 5: randomAdj = "peppy";
      break;
      case 6: randomAdj = "slim";
      break;
      case 7: randomAdj = "dirty";
      break;
      case 8: randomAdj = "snooty";
      break;
      case 9: randomAdj = "melancholy";
      break;
      default: randomAdj = "steamy";
    }
    return randomAdj; //Returns the random word
  }
  
  public static String subject() { //Same as above
    Random randomGeneratorS = new Random();
    int randomNumSub = randomGeneratorS.nextInt(10);
    String randomSub;
    switch (randomNumSub) {
      case 1: randomSub = "carpenter";
      break;
      case 2: randomSub = "mailperson";
      break;
      case 3: randomSub = "wrestler";
      break;
      case 4: randomSub = "astronaut";
      break;
      case 5: randomSub = "blacksmith";
      break;
      case 6: randomSub = "cop";
      break;
      case 7: randomSub = "usher";
      break;
      case 8: randomSub = "bouncer";
      break;
      case 9: randomSub = "leper";
      break;
      default: randomSub = "bartender";
    }
    return randomSub;
  }
  
  public static String verb() {
    Random randomGeneratorV = new Random();
    int randomNumVerb = randomGeneratorV.nextInt(10);
    String randomVerb;
    switch (randomNumVerb) {
      case 1: randomVerb = "jumped";
      break;
      case 2: randomVerb = "thanked";
      break;
      case 3: randomVerb = "tipped";
      break;
      case 4: randomVerb = "pitied";
      break;
      case 5: randomVerb = "betrayed";
      break;
      case 6: randomVerb = "tickled";
      break;
      case 7: randomVerb = "high-fived";
      break;
      case 8: randomVerb = "flattered";
      break;
      case 9: randomVerb = "complimented";
      break;
      default: randomVerb = "slapped";
    }
    return randomVerb;
  }
  
   public static String object() {
    Random randomGeneratorO = new Random();
    int randomNumObj = randomGeneratorO.nextInt(10);
    String randomObj;
    switch (randomNumObj) {
      case 1: randomObj = "Cool Ranch Doritos Locos Taco";
      break;
      case 2: randomObj = "human boy";
      break;
      case 3: randomObj = "rat";
      break;
      case 4: randomObj = "trained assassin";
      break;
      case 5: randomObj = "plant";
      break;
      case 6: randomObj = "dog";
      break;
      case 7: randomObj = "anthropomorphic bird";
      break;
      case 8: randomObj = "frog";
      break;
      case 9: randomObj = "lizard";
      break;
      default: randomObj = "balloon";
    }
    return randomObj;
  }
    
  } //End of class