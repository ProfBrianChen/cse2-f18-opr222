//Owen Rahr 9/18/2018
//hw03: Converter

import java.util.Scanner; //Imports a scanner to detect user inputs

public class Convert{
  public static void main(String[] args) { //The main method for every program
    
    Scanner myScanner = new Scanner( System.in ); //Declaring a new scanner to take input
    
    System.out.print("Enter the affected area in acres: "); //Prompts the user for the number of acres
    double areaAcres = myScanner.nextDouble(); //Records the value as the double areaAcres
    System.out.print("Enter the amount of rainfall in the affected area: "); //Prompts the user for the inches of rainfall
    double amountOfRain = myScanner.nextDouble(); //Records the value as the double amountOfRain
    
    float cubicMiles = (float)((areaAcres * 0.0015625) * (amountOfRain / 63360)); //Calculates the quantity of rain to cubic miles
    System.out.println(cubicMiles + "cubic miles."); //Displays the amount of cubic miles
    
  }
}