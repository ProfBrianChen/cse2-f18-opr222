//Owen Rahr 9/18/2018
//hw03: Pyramid volume calculator

import java.util.Scanner; //Imports a scanner

public class Pyramid{
  public static void main(String[] args) { //The main method for every program
    
    Scanner myScanner = new Scanner( System.in ); //Declares the scanner to be used in the program
    
    System.out.print("Enter the width of one of the pyramid's sides: "); //Prompts user for width
    double pyramidWidth = myScanner.nextDouble(); //Declares and records the input for the width
    System.out.print("Enter the height of the pyramid: "); //Prompts user for height
    double pyramidHeight = myScanner.nextDouble(); //Declares and records the input for the height
    
    int pyramidVolume = (int)(pyramidWidth * pyramidWidth * pyramidHeight / 3); //Declares and calculates volume, then converts it to an integer
    System.out.println("The pyramid's volume is " + pyramidVolume + "."); //Displays the result
    
  }
}
    