//Owen Rahr 10/18/2018 CSE002
//Hw06: Encrypted X
//This program generates a square with an encrypted x inside, as big as the user prompts

import java.util.Scanner; //Imports a scanner

public class EncryptedX {
  public static void main(String[] args) { //Main method of the program
    
    Scanner scan = new Scanner(System.in); //Initializes a scanner
    int squareSize = 0; //Initializes the variable for square size outside of the do while loop
    do { //Start of a do while loop
      System.out.print("Enter the size square to print(1-100): "); //User prompt for square size
      boolean correct = scan.hasNextInt(); //Records user input, and sets a boolean value for if it's an int
      while (!correct) { //While loop that runs if the user didn't input an integer
        System.out.print("Incorrect type, enter again: "); //Error message
        scan.next(); //resets the scanner so it can record a new variable
        correct = scan.hasNextInt(); //Performs the same action as 14
      }
      squareSize = scan.nextInt(); //The user input that is confirmed to be an int is set to squaresize
    } while (squareSize < 1 || squareSize > 100); //Checks if the value is in range, if not, the loop repeats
    
    for (int i = 0; i < squareSize; ++i) { //A for loop that runs once for each column, up to squareSize
      for (int j = 0; j < squareSize; ++j) { //Loop that runs for each row, printing as many "*"s and " "s as squareSize
        if (j == i || squareSize - (j + 1) == i) { //If the row and column are at the same value, a space should be printed.
          System.out.print(" "); 
        }
        else {
          System.out.print("*"); //What is printed if a space is not printed.
        } //end of if else statement
      } //end of the interior for loop
      System.out.println(); //Sets it to the next row
    } //end of the exterior for loop
   
  } //end of main method
} //end of class