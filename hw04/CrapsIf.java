//Owen Rahr 9/25/2018 CSE002
//hw04: Craps If
//This program either generates or records dice values and then prints what slang terminology is used for them in craps.

import java.util.Scanner; //Imports a scanner to be declared later

public class CrapsIf{
  public static void main(String[] args){ //Main method for every program
    
    Scanner myScanner = new Scanner(System.in); //Declares a scanner to record input
    int dieOne = -1; //Declares the integers that will represent each die's value
    int dieTwo = -1; //They are assigned dummy variables here to avoid a compiler error
    
    System.out.println("Enter 'random' if you want the dice to be randomly generated or 'input' if you want to input your own values");
    //Prompts user for what type of input they want to use
    String inputType = myScanner.nextLine(); //Declares a variable that will be scanned

    if (inputType.equals("random")) { //If user inputted random for the type they wanted it assigns values for the dice
      dieOne = (int)(Math.random()*6 + 1); //Randomly assigns a value between 1 and 6 for the dice
      dieTwo = (int)(Math.random()*6 + 1);
    }
    else if (inputType.equals("input")) { //If user inputted input for the type they wanted, it runs code to record the values
      System.out.println("Input the first die's value: "); //Prompts user for first value
      dieOne = myScanner.nextInt(); //Detects and assigns input to dieOne
      System.out.println("Input the second die's value: "); //Prompts user for second value
      dieTwo = myScanner.nextInt(); //Detects and assigns input to dieTwo
    }
    else { //When the user's input does not match either case, it prints 'invalid' and ends the program
      System.out.println("Invalid type entry");
      System.exit(0);
    }
    
    if ((0 < dieOne && dieOne < 7) && (0 < dieTwo && dieTwo < 7)) { //Checks to see if the values are in range
    }
    else { //If they aren't in range, it prints 'invalid' and ends the program
    System.out.println("Invalid die values");
    System.exit(0);
    }
    
    System.out.print("The slang term for your roll is: ");//Prints a lead-in statement before the slang is determined
      
    /*The rest of the code is a series of nested if else statements that determine the slang term for each roll.
    For each possible sum of the dice, the slang term is printed if there is only one for the roll, or if there
    is more than one possible term for a sum, then another if statement checks for it and prints what term it is.*/
    
    if (dieOne + dieTwo == 2) { //Checks for if the sum is 2. If so, the only possible output, "Snake Eyes" is printed
      System.out.println("Snake Eyes");
    }
    
    else if (dieOne + dieTwo == 3) { //Repeats the process of lines 44-46 for each possible sum value
      System.out.println("Ace Deuce");
    }
   
    else if (dieOne + dieTwo == 4) { //In the case that there is more than one possible output for a sum another if statement is used
      if (dieOne == 2) {  //Only one value needs to be 2 in order to yield "Hard Four" so it only checks dieOne
        System.out.println("Hard Four");
      }
      else { //All other combinations yield "Easy Four", so they don't need to be checked for
        System.out.println("Easy Four");
      }
    } //This checking process is repeated when the sum is 6, 8, or 10
    
    else if (dieOne + dieTwo == 5) {
      System.out.print("Fever Five");
    }
    
    else if (dieOne + dieTwo == 6) {
      if (dieOne == 3) {
        System.out.println("Hard Six");
      }
      else {
        System.out.println("Easy Six");
      }
    }
    
    else if (dieOne + dieTwo == 7) {
      System.out.println("Seven Out");
    }
    
    else if (dieOne + dieTwo == 8) {
      if (dieOne == 4) {
        System.out.println("Hard Eight");
      }
      else {
        System.out.println("Easy Eight");
      }
    }
    
    else if (dieOne + dieTwo == 9) {
      System.out.println("Nine");
    }
    
    else if (dieOne + dieTwo == 10) {
      if (dieOne == 5) {
        System.out.println("Hard Ten");
      }
      else {
        System.out.println("Easy Ten");
      }
    }
    
    else if (dieOne + dieTwo == 11) {
      System.out.println("Yo-leven");
    }
    
    else if (dieOne + dieTwo == 12) {
      System.out.println("Box Cars");
    }
       
  }
}