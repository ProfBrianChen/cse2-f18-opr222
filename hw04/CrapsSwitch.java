//Owen Rahr 9/25/2018 CSE002
//hw04: Craps Switch
//This program either generates or records dice values and then prints what slang terminology is used for them in craps.

import java.util.Scanner; //Imports a scanner to be declared later

public class CrapsSwitch{
  public static void main(String[] args){ //Main method for every java program
    
    Scanner myScanner = new Scanner(System.in); //Declares a scanner to record input
    int dieOne = -1; //Declares the integers that will represent each die's value
    int dieTwo = -1; //They are assigned dummy variables here to avoid a compiler error
    
    System.out.println("Enter 'random' if you want the dice to be randomly generated or 'input' if you want to input your own values");
    //Prompts user for what type of input they want to use
    String inputType = myScanner.nextLine(); //Declares a variable that will be scanned
    switch (inputType) { //Creates a switch that performs different tasks based on what input is chosen
      case "random": //When the user chooses random, it generates random numbers for each variable
        dieOne = (int)(Math.random()*6 + 1);
        dieTwo = (int)(Math.random()*6 + 1);
        break;
      case "input": //Shows what happens when user chooses input
        System.out.print("Input the first die's value: "); //Prompts user for first value
        dieOne = myScanner.nextInt(); //Detects and assigns input to dieOne
        switch (dieOne) { //For each valid value, the switch prompts and detects user input for dieTwo
          case 1: 
            System.out.print("Input the second die's value: ");
            dieTwo = myScanner.nextInt();
            break;
          case 2:
            System.out.print("Input the second die's value: ");
            dieTwo = myScanner.nextInt();
            break;
          case 3:
            System.out.print("Input the second die's value: ");
            dieTwo = myScanner.nextInt();
            break;
          case 4:
            System.out.print("Input the second die's value: ");
            dieTwo = myScanner.nextInt();
            break;
          case 5:
            System.out.print("Input the second die's value: ");
            dieTwo = myScanner.nextInt();
            break;
          case 6:
            System.out.print("Input the second die's value: ");
            dieTwo = myScanner.nextInt();
            break;
          default: //In the case that an invalid value is chosen, the program will be stopped
            System.out.println("Not in range");
            System.exit(0);
            break;
        }
        switch (dieTwo) { //This switch also detects whether the value is valid, and ends the program if otherwise
          case 1:
            break;
          case 2:
            break;
          case 3:
            break;
          case 4:
            break;
          case 5:
            break;
          case 6:
            break;
          default:
            System.out.println("Not in range");
            System.exit(0);
        }
        break;
      default: 
        System.out.println("invalid entry"); //States invalid entry and ends the program if an invalid response for input type is given
        System.exit(0);
        break;
    }
    //Now that the dice values are assigned, the slang terminology can be determined
    
    System.out.print("The slang terminology for your roll is: "); //Prints a lead-in statement before the slang is determined
    
    switch (dieOne) { /*This is a larger switch for every value the first die can have. For each value(1-6), there is another 
    switch for the value(1-6) of the second die, that will print out the proper slang term for the dice values*/
      
      case 1: //First case for dieOne
        switch (dieTwo) { //Switch that determines and prints the combination with one
          case 1:
            System.out.println("Snake Eyes");
              break;
          case 2:
            System.out.println("Ace Deuce");
            break;
          case 3:
            System.out.println("Easy Four");
            break;
          case 4:
            System.out.println("Fever Five");
            break;
          case 5:
            System.out.println("Easy Six");
            break;
          case 6:
            System.out.println("Seven Out");
            break;
          default:
          } //End of the switch for dieTwo
        break; //End of the first case for dieOne
        
      case 2: //Second case for dieOne
        switch (dieTwo) {
          case 1: 
            System.out.println("Ace Duece");
            break;
          case 2:
            System.out.println("Hard Four");
            break;
          case 3:
            System.out.println("Fever Five");
            break;
          case 4:
            System.out.println("Easy Six");
            break;
          case 5:
            System.out.println("Seven Out");
            break;
          case 6:
            System.out.println("Easy Eight");
            break;
          default:
          } //End of the switch for dieTwo
        break; //End of the second case for dieOne
        
      case 3: //Third case for dieOne
        switch (dieTwo) {
            case 1: 
            System.out.println("Easy Four");
            break;
          case 2:
            System.out.println("Fever Five");
            break;
          case 3:
            System.out.println("Hard Six");
            break;
          case 4:
            System.out.println("Seven Out");
            break;
          case 5:
            System.out.println("Easy Eight");
            break;
          case 6:
            System.out.println("Nine");
            break;
          default:
          } //End of the switch for dieTwo
        break; //End of the third case for dieOne
        
      case 4: //Fourth case for dieOne
        switch (dieTwo) {
        case 1: 
            System.out.println("Fever Five");
            break;
          case 2:
            System.out.println("Easy Six");
            break;
          case 3:
            System.out.println("Seven Out");
            break;
          case 4:
            System.out.println("Hard Eight");
            break;
          case 5:
            System.out.println("Nine");
            break;
          case 6:
            System.out.println("Easy Ten");
            break;
          default:
        } //End of the switch for dieTwo
        break; //End of the fourth case for dieOne
        
      case 5: //Fifth case for dieOne
        switch (dieTwo) {
            case 1: 
            System.out.println("Easy Six");
            break;
          case 2:
            System.out.println("Seven Out");
            break;
          case 3:
            System.out.println("Easy Eight");
            break;
          case 4:
            System.out.println("Nine");
            break;
          case 5:
            System.out.println("Hard Ten");
            break;
          case 6:
            System.out.println("Yo-leven");
            break;
          default:
        } //End of the switch for dieTwo
        break; //End of the fifth case for dieOne
        
      case 6: //Sixth case for dieOne
        switch (dieTwo) {
            case 1: 
            System.out.println("Seven Out");
            break;
          case 2:
            System.out.println("Easy Eight");
            break;
          case 3:
            System.out.println("Nine");
            break;
          case 4:
            System.out.println("Easy Ten");
            break;
          case 5:
            System.out.println("Yo-leven");
            break;
          case 6:
            System.out.println("Box Cars");
            break;
          default:
        } //End of the switch for dieTwo
        break; //End of the sixth case for dieOne
      
      default: //Default case for dieOne
        
    } //End of the switch for dieOne
    
  }
}