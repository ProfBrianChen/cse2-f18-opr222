//Owen Rahr 10/28/2018 CSE002
//Hw07: Word Tools 
//This program takes a phrase that the user inputs and performs different modifications
//or analyses based on what the user inputs

import java.util.Scanner; //Imports a scanner

public class wordTools { //Start of the class
 
  public static void main(String[] args) { //Start of the main method
    String userInput = sampleText(); //Assigns a string for the user's input using method sampleText
    printMenu(userInput); //Passes the input to the printMenu method, where most of the magic happens
  }
  
  public static String sampleText() { //The method that records the initial phrase
   Scanner scan1 = new Scanner(System.in); //Declaring/initializing a scanner
   System.out.println("Enter a sample text: "); //Prompt
   String sampleText = (String)scan1.nextLine(); //Scanner records the user's input
   System.out.println("\nYou entered: " + sampleText); //Prints the input
   return sampleText; //Returns the phrase to the main method
  } //End of sampleText method
  
  public static void printMenu(String userInput) { //Start of the printMenu method
    
    //The next three lines print the menu and all of the shortcuts available to the user
    System.out.println("MENU\nc - Number of non-whitespace characters\nw - Number of words");
    System.out.println("f - Find text\nr - Replace all !'s\ns - Shorten spaces\nq - quit");
    System.out.println("\nChoose an option:");
    
    String response; //Declares a string for the user's response
    Scanner scan2 = new Scanner(System.in); //Declares a scanner for the user's menu choice
    response = scan2.next(); //Records user choice as response
    
    if (response.equals("q")){ //If the response is 'q', the program is terminated
      System.exit(0);
    }
    else if (response.equals("c")) { //If the response is 'c', the getNumOfNonWSCharacters method is called, using userInput
      getNumOfNonWSCharacters(userInput);
    }
    else if (response.equals("w")) { //If the response is 'w', the getNumOfWords method is called, using userInput
      getNumOfWords(userInput);
    }
    else if (response.equals("f")) { //If the response is 'f', the findText method is called, using userInput
      findText(userInput);
    }
    else if (response.equals("r")) { //If the response is 'r', the replaceExclamation method is called, using userInput
      userInput = replaceExclamation(userInput); //This case and the next both modify the statement, so userInput is replaced with the return value
      System.out.println("Edited text: " + userInput); //These two also both print out the new text
      printMenu(userInput); //Recalls printMenu with the modified userInput
    }
    else if (response.equals("s")) { //If the response is 's', the shortenSpace method is called, using userInput
      userInput = shortenSpace(userInput);
      System.out.println("Edited text: " + userInput);
      printMenu(userInput);
    }
    else {
      printMenu(userInput); //If an ivalid response is given, the printMenu method is recalled, passing userInput
    }
  } //End of printMenu method
  
 
  //This method finds the number of non-whitespace characters in the phrase
  public static void getNumOfNonWSCharacters(String userInput) {
    int numOfNonWSCharacters = 0; //Initializes a variable for numOfNonWSCharacters as 0 to avoid a scope issue
    for (int i = 0; i < userInput.length(); i++) { //Runs through a loop once for each character of the phrase
      if (!Character.isWhitespace(userInput.charAt(i))) { //Checks if the character at what value the loop is at isn't a whitespace
        numOfNonWSCharacters++; //If it is, it increments it
      }
    } //End of loop
    System.out.println("Number of non-whitespace characters: " + numOfNonWSCharacters); //Gives response
    printMenu(userInput); //Calls printMenu
  } //End of getNumOfNonWSCharacters
  
 
  //This method finds the number of words in the phrase
  public static void getNumOfWords(String userInput) {
    int numOfWords = 0; //Initializes variable for number of words as 0
    for (int i = 1; i < userInput.length(); i++) { //Loop runs once for every value from the second letter to the last
      
      //The following if statement is entered if the character before i is a letter and i is a space
      if (Character.isLetter(userInput.charAt(i-1)) && Character.isWhitespace(userInput.charAt(i))) {
        numOfWords++; //If this condition is met, numOfWords is incremented
      }
      //The next 4 if statements are entered if a '.' ',' '!' or '?' follow a letter
      else if (Character.isLetter(userInput.charAt(i-1)) && userInput.charAt(i) == 33) {
        numOfWords++;
      }
      else if (Character.isLetter(userInput.charAt(i-1)) && userInput.charAt(i) == 44) {
        numOfWords++;
      }
      else if (Character.isLetter(userInput.charAt(i-1)) && userInput.charAt(i) == 46) {
        numOfWords++;
      }
      else if (Character.isLetter(userInput.charAt(i-1)) && userInput.charAt(i) == 63) {
        numOfWords++;
      }
      //If the last character is a letter, numOfWords is incremented
      else if (Character.isLetter(userInput.charAt(i)) && i == userInput.length() - 1) {
        numOfWords++;
      }
      
    } //End of for loop
    
    System.out.println("Number of words: " + numOfWords); //Prints the result
    printMenu(userInput); //Runs the printMenu method
  } //End of getNumOfWords method
  
  
  //This next method finds the number of times a phrase of choice appears in the phrase
  public static void findText(String userInput) {
    
    System.out.println("Enter a word or phrase to be found:"); //Prompt
    Scanner scan3 = new Scanner(System.in); //Declares a scanner
    String targetPhrase = (String)scan3.nextLine(); //Records the target phrase
    int numInstances = 0; //Initializes the counter for number of instances as 0
    
    int i = -1; //Initializes the variable i which keeps track of if there are more instances of targetphrase
    do {
      i = userInput.indexOf(targetPhrase, i + 1); //If there is an instance of the target phrase after what value i is,
                                                  //i will be set to what number letter the phrase appears. otherwise, it will be -1
      if (i > -1) { //If there was an instance of the target phrase, numInstances will be incremented
        numInstances++;
      }
    } while (i > -1); //The loop continuously runs until the phrase is no longer found
    
    System.out.println("\"" + targetPhrase + "\" instances: " + numInstances); //Prints result
    printMenu(userInput); //Runs printMenu method
  } //End of findText method
  
  
  //This method replaces any exclamation points with periods
  public static String replaceExclamation(String userInput) {
    return userInput.replace('!', '.'); //.replace replaces all '!'s with '.'s. It returns this value to printMenu
  } //End of replaceExclamation method
  
  
  //This method shortens any spaces that are larger than one
  public static String shortenSpace(String userInput) {
    while (userInput.indexOf("  ") > -1) { //If there are any double spaces, the loop is entered
      userInput = userInput.replace("  ", " "); //Replaces all instances of double space with a single space
    } //End of loop
    return userInput; //Returns the edited userInput
  } //End of shortenSpace method
  
} //End of class