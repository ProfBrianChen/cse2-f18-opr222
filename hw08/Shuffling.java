//Owen Rahr CSE002 11/13/2018
//HW08: Card Shuffler
//This program uses arrays to shuffle a 'deck' of 52 cards.

import java.util.Scanner; //Imports scanner
import java.util.Random;  //Imports random class

public class Shuffling{ //public class of the method
  
  public static void printArray(String[] array) { //This method prints the input array
    for (int i = 0; i < array.length; i++) { //Counter runs once for each element in an array
      System.out.print(array[i] + " "); //Prints the content of the array at i
    }
    return;
  } //End of printArray method
  
  public static void shuffle(String[] cards) { //This method shuffles the cards
    Random random = new Random(); //Declares a random generator
    String holder; //A holder variable to swap values with
    int randomCard; //An int to represent what card value to switch
    for (int i = 0; i < 100; i++) { //The loop runs 100 times, switching a random card with the first card
      randomCard = random.nextInt(52); //New random number to represent a card
      holder = cards[randomCard]; //Next three lines are switching the card at 0 and the random number
      cards[randomCard] = cards[0];
      cards[0] = holder;
    }
    return;
  } //End of shuffle method
  
  public static String[] getHand(String[] cards, int index, int numCards) { //This method prints out a hand
    String[] hand = new String[numCards]; //Creates an array for a hand
    int j = 0; //Counter
    for (int i = index; i > (index - numCards); i--, j++) { //Loop runs from the index(where the current hand is at)
      //and runs as many times as numCards, or size of hand. 
      hand[j] = cards[i]; //j increases as large as size of hand, and i decreases from index. This assigns the proper cards to the hand
    }
    return hand;
  } //end of gethand method

  public static void main(String[] args) { //Start of main method
    Scanner scan = new Scanner(System.in); //Initializing a scanner
    //suits club, heart, spade or diamond 
    String[] suitNames={"C","H","S","D"}; //Declaring an array of suitnames
    String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; //The same for card rank
    String[] cards = new String[52]; //Declares the array for the deck
    String[] hand = new String[5]; //Declares the array for the hand
    int numCards = 5; //Number of cards value
    int again = 1; //Int to check for a while loop
    int index = 51; //Index tracks at what card to draw from, so as not to copy a card twice
    
    for (int i=0; i<52; i++){ //Loop that sets the deck to an unshuffled version
      cards[i]=rankNames[i%13]+suitNames[i/13]; //Assigns card values
    }
    
    System.out.println();
    printArray(cards); //Passes the cards to printArray
    System.out.println("\nShuffled");
    shuffle(cards); //Shuffles cards
    printArray(cards); //Prints the shuffled cards
    System.out.println("\nHand");
    while(again == 1){ //This while loop prints out the hand, can be repeated if the user inputs 1
      if (index < numCards) { //In case there arent enough cards this makes a new deck
        System.out.println("New deck: ");
        shuffle(cards); //Reshuffles the deck
        printArray(cards); //Prints the new deck
        index = 51; //Resets the index
        System.out.println();
      }        
      hand = getHand(cards,index,numCards); //Sets the hand array equal with getHand
      printArray(hand); //Prints the hand
      index = index - numCards; //Sets the index to the proper value
      
      System.out.println("Enter a 1 if you want another hand drawn"); 
      again = scan.nextInt(); //Scans for if the user wants another hand drawn
    } //End of while loop
    
  } //End of main method
} //End of class
 