//Owen Rahr 9/10/18 CSE002
//hw02: Arithmetic Calculations

public class Arithmetic{
  public static void main(String[] args){
    
    //The given variables needed will be declared next
   
    int numPants = 3;  //Number of pairs of pants
    double pantPrice = 34.98; //Cost per pair of pants
    int numShirts = 2; //Number of sweatshirts
    double shirtPrice = 24.99; //Cost per shirt
    int numBelts = 1; //Number of belts
    double beltPrice = 33.99; //cost per belt
    double paSalesTax = 0.06; //the tax rate

    //The variables that will be calculated will be declared next
    
    double totalCostOfPants; //total cost of pants
    double totalCostOfShirts; //total cost of shirts
    double totalCostOfBelts; //total cost of belts
    double salesTaxOfPants; //sales tax of pants
    double salesTaxOfShirts; //sales tax of shirts
    double salesTaxOfBelts; //sales tax of belts
    double totalCostOfPurchases; //total cost of purchases
    double totalSalesTax; //total sales tax
    double netTotal; //total paid for the transaction, including sales tax
    
    //The values will be calulated next
    
    totalCostOfPants = numPants * pantPrice; //Calculates cost of pants
    totalCostOfShirts = numShirts * shirtPrice; //Calculates cost of shirts
    totalCostOfBelts = numBelts * beltPrice; //Calculates cost of belts
    salesTaxOfPants = totalCostOfPants * paSalesTax; //Calculates the tax paid on pants
    salesTaxOfShirts = totalCostOfShirts * paSalesTax; //Calculates the tax paid on shirts
    salesTaxOfBelts = totalCostOfBelts * paSalesTax; //Calculates the tax paid on belts
    
    //The sales tax values will need to be maipulated to only show two decimal places
    //This will results in a loss of precision, and cents will not be rounded up
    
    salesTaxOfPants *= 100;
    salesTaxOfPants = (int)salesTaxOfPants / 100.0;
    salesTaxOfShirts *= 100;
    salesTaxOfShirts = (int)salesTaxOfShirts / 100.0;
    salesTaxOfBelts *= 100;
    salesTaxOfBelts = (int)salesTaxOfBelts / 100.0;
    
    //The rest of the values will be calculated
    
    totalCostOfPurchases = totalCostOfPants + totalCostOfShirts + totalCostOfBelts; //Calculates total cost of purchases
    totalSalesTax = salesTaxOfPants + salesTaxOfShirts + salesTaxOfBelts; //Calculates total sales tax paid
    netTotal = totalCostOfPurchases + totalSalesTax; //Calculates total money spent
    
    //The prices will be printed, using the values calculated
    
    System.out.println("Three pairs of pants were bought that costed $" + pantPrice + " each. The total sales tax paid for them was $" + salesTaxOfPants);
    System.out.println("The total cost of pants was $" + totalCostOfPants);
    System.out.println("Two shirts were bought that costed $" + shirtPrice + " each. The total sales tax paid for them was $" + salesTaxOfShirts);
    System.out.println("The total cost of shirts was $" + totalCostOfShirts);
    System.out.println("One belt was bought that costed $" + beltPrice + ". The total sales tax paid for it was $" + salesTaxOfBelts);  
    System.out.println("The total cost of the purchases before tax was $" + totalCostOfPurchases);
    System.out.println("The total sales tax paid was $" + totalSalesTax);
    System.out.println("The total money spent was $" + netTotal);
    
  }
}