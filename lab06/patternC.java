//Owen Rahr 10/11/2018 CSE002
//Lab06: Display Pryamids
//This program displays two dimensional patterns using nested loops

import java.util.Scanner;

public class patternC {
  public static void main(String[] args) { //Public class for the program
    
    Scanner scan = new Scanner(System.in); //Imports a scanner
    int pyramidLength = 0; //This variable records what size pyramid will be printed
    
    do { //Start of the do while loop that ensures that a proper value is recorded
      
    System.out.print("Enter an integer 1-10: "); //Prompts for a value
    
    boolean intTest = scan.hasNextInt(); //Assigns a boolean value based on user's input
    while (!intTest){ //If that boolean value is false, it means that it wasn't an int, so this loop will run
      System.out.print("Incorrect value, enter again: "); //error message
      scan.next(); //resets coneyor belt
      intTest = scan.hasNextInt(); //Reassigns the boolean value with the user's next input
    }
    pyramidLength = scan.nextInt(); //Once an integer is given, it will be assigned to pyramidLength
   
    }  while (pyramidLength < 1 || pyramidLength > 10); //If the integer value is not in range, the entire loop repeats

    
    for (int i = 1; i <= pyramidLength; i++) { //int i tracks the max value printed for each line from 1-pyramidLength
        for (int j = i; j >= 1; j--){ //for every line, j decreases from i-1, 
          if (j == i){ //Before the first value for the j loop, extra spaces are filled in to offset everything right
            if (i < 10){ //Before the tenth line, an extra space is printed, to account for that 10 has 2 characters
              System.out.print(" "); //The extra space from above
            }
          for (int k = pyramidLength; k > j; k--){ //int k runs as many times as the difference between the pyramidLength and j
            System.out.print(" "); //Prints a space each time
            }
          }
        System.out.print(j); //Prints j every time the 'j' loop is run
      }
      System.out.println(); //Everytime the 'j' loop finishes, a new line is printed, to separate them
    }
    
  }
}
    