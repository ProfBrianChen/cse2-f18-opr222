//Owen Rahr 10/11/2018 CSE002
//Lab06: Display Pryamids
//This program displays two dimensional patterns using nested loops

import java.util.Scanner;

public class patternB {
  public static void main(String[] args) { //Public class for the program
    
    Scanner scan = new Scanner(System.in); //Imports a scanner
    int pyramidLength = 0; //This variable records what size pyramid will be printed
    
    do { //Start of the do while loop that ensures that a proper value is recorded
      
    System.out.print("Enter an integer 1-10: "); //Prompts for a value
    
    boolean intTest = scan.hasNextInt(); //Assigns a boolean value based on user's input
    while (!intTest){ //If that boolean value is false, it means that it wasn't an int, so this loop will run
      System.out.print("Incorrect value, enter again: "); //error message
      scan.next(); //resets coneyor belt
      intTest = scan.hasNextInt(); //Reassigns the boolean value with the user's next input
    }
    pyramidLength = scan.nextInt(); //Once an integer is given, it will be assigned to pyramidLength
   
    }  while (pyramidLength < 1 || pyramidLength > 10); //If the integer value is not in range, the entire loop repeats
    
    for (int i = pyramidLength; i >= 1; i--) { //int i tracks the line number and width of the pattern. It decreases from pyramidLength-1
        for (int j = 1; j <= i; j++){ //int j increases from 1-i, at whichever value i is for the loop
        System.out.print(j + " "); //Everytime the 'j' loop is run, it prints j's value
      }
      System.out.println(); //Everytime the 'j' loop finishes, a new line is printed, to separate them
    }
    

  }
}
    