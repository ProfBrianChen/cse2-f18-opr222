//Owen Rahr CSE002 11/15/2018
//Lab08: Methods and Arrays

import java.util.*;


public class lab09 {
  public static void main(String[] args) {
  
    int[] array0 = new int[] {1,2,3,4,5,6,7,8,9};
    int[] array1 = copy(array0);
    int[] array2 = copy(array0);
    inverter(array0);
    print(array0);
    inverter2(array1);
    print(array1);
    int[] array3 = inverter2(array2);
    print(array3);
  }
  
  public static int[] copy(int[] arrayC) {
    int[] arrayC2 = new int[arrayC.length];
    for (int i = 0; i < arrayC.length; i++) {
      arrayC2[i] = arrayC[i];
    }
    return arrayC2;
  }
  
  public static void inverter(int[] arrayI) {
    int j = (arrayI.length - 1);
    int holder = 0;
    for (int i = 0; i < (arrayI.length)/2; i++, j--) {
      holder = arrayI[i];
      arrayI[i] = arrayI[j];
      arrayI[j] = holder;
    }
    return;
  }
  
  public static int[] inverter2(int[] arrayI2) {
    int[] arrayI2C = copy(arrayI2);
    int j = (arrayI2C.length - 1);
    int holder = 0;
    for (int i = 0; i < (arrayI2C.length)/2; i++, j--) {
      holder = arrayI2C[i];
      arrayI2C[i] = arrayI2C[j];
      arrayI2C[j] = holder;
    }
    return arrayI2C;
  }
  
  public static void print(int[] arrayP) {
    for (int i = 0; i < arrayP.length; i++) {
      System.out.print(arrayP[i] + " ");
    }
    System.out.println();
    return;
  }
    
  
}
  