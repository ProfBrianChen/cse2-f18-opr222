//Owen Rahr Cse002 11/29
//Hw09: remove elements
//This program generates an array, then removes specifie values and indexes.


import java.util.*; //imports scanner and random


public class removeElements{
  
  public static void main(String [] arg){ //Main class provided in hw
    Scanner scan=new Scanner(System.in);
    int num[]=new int[10];
    int newArray1[];
    int newArray2[];
    int index,target;
    String answer = "";
    do {
      System.out.print("Random input 10 ints [0-9]");
      num = randomInput();
      String out = "The original array is:";
      out += listArray(num);
      System.out.println(out);
 
      System.out.print("Enter the index ");
      index = scan.nextInt();
      newArray1 = delete(num,index);
      String out1 = "The output array is ";
      out1 += listArray(newArray1); //return a string of the form "{2, 3, -9}"  
      System.out.println(out1);
 
      System.out.print("Enter the target value ");
      target = scan.nextInt();
      newArray2 = remove(num,target);
      String out2="The output array is ";
      out2 += listArray(newArray2); //return a string of the form "{2, 3, -9}"  
      System.out.println(out2);
   
      System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
      answer=scan.next();
    } while(answer.equals("Y") || answer.equals("y"));
  }
  
  public static int[] randomInput() {
    int[] randomInputs = new int[10]; //declares an array of integers
    Random random = new Random(); //declares a random generator
    for (int i = 0; i < 10; i++) { //for every index of the array
      randomInputs[i] = random.nextInt(10); //set the value to a random number 1-10
    }
    return randomInputs; //Returns array
  }
  
  public static int[] delete(int[] list, int pos) {
    int[] deleteList = new int[list.length - 1]; //New array 1 smaller than before, because an element will be removed
    if (pos < 0 || pos > 9) {
      System.out.println("Not a valid index.");
      return list;
    }
    int deleteCounter = 0; //Counter for what index the shorter method is at
    for (int originalCounter = 0; originalCounter < 10; originalCounter++, deleteCounter++) { //runs 10 times, incrementing both counters
      if (originalCounter == pos) { //Where the index is specified
        deleteCounter--; //Sets the delete counter one back, so it skips this value of the original list
      } else {
        deleteList[deleteCounter] = list[originalCounter]; //Otherwise just copies the normal list
        }
      }
    return deleteList;
  }
  
  public static int[] remove(int[] list1, int target) {
    int numTargets = 0; //Counter for number of times the target value appears
    for (int i = 0; i < list1.length; i++) { //Loop that increments numTargets for every instance of target
      if (list1[i] == target) {
        numTargets++;
      }
    }
    int[] removeList = new int[(list1.length - numTargets)]; //Sets the new array to a length that excludes the targets
   
    int removeCounter = 0; //Similar practice as delete method, counting the indexes for each array separately
    for (int originalCounter = 0; removeCounter < removeList.length; originalCounter++, removeCounter++) {
      if (list1[originalCounter] == target) {
        removeCounter--; //Sets the index one back if it is the target value
      } else {
        removeList[removeCounter] = list1[originalCounter]; //Sets the other values equal
        }
      }
    return removeList;
  }
        
      
 
  public static String listArray(int num[]){ //Provided with the hw
    String out="{";
    for(int j=0;j<num.length;j++){
      if(j>0){
        out+=", ";
      }
      out+=num[j];
    }
    out+="} ";
    return out;
  }
}
 