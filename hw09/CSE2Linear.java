//Owen Rahr CSE002 11/28
//HW09: Linear Search

import java.util.*; //imports necessary methods

public class CSE2Linear {
 
  public static void main(String[] args) {
    
    int[] grades = new int[15]; //Array for grades
    int currGrade = -1; //tracker for the grade input
    Scanner scan = new Scanner(System.in); //declaring scanner
    boolean check = false; //boolean that can be changed from several things, all resetting the following loop
    
    
    System.out.println("Print a list of ascending integers from 0-100, for a student's final grades. ");
    for (int i = 0; i < grades.length; i++) { //Runs once for each index in the array
      
      do {
        check = true; //resets boolean
        System.out.print("Enter grade: ");
        check = scan.hasNextInt(); //if an integer isn't inputed, check will be set to false
        while (!check) { //This loop will run til an integer is submitted
          System.out.println("Error: not an int");
          System.out.print("Enter grade: ");
          scan.next();
          check = scan.hasNextInt();
        }
        currGrade = scan.nextInt(); //Sets temp grade to inputted value
        if (currGrade < 0 || currGrade > 100) { //If out of range, boolean will be set to false
          System.out.println("Error: not in range 1-100");
          check = false;
        } else if (i > 0 && currGrade < grades[i-1]) { //if the grade is less than the previous, then boolean will be set to false
          System.out.println("Error: grade is less than previous");
          check = false;
        } else {
          check = true;
        }
      } while (!check); //If anything made it false, then the loop will repeat
      grades[i] = currGrade; //Sets the good value
    
    }
    printArray(grades); //Calls a method that prints the array
    
    System.out.print("\nEnter a grade to search for: ");
    int targetGrade = scan.nextInt(); //Gets value to search for
    binarySearch(targetGrade, grades); //Uses a method to binary search
    scramble(grades); //Uses a method to scramble the grades
    System.out.println("Scrambled");
    printArray(grades); 
    System.out.print("\nEnter a grade to search for: ");
    targetGrade = scan.nextInt();
    linearSearch(targetGrade, grades); //Uses a method to search linearly
    
  }
  
  public static void printArray(int[] array) { //Method runs a for loop, printing each value of an array
    for (int i = 0; i < 15; i++) {
      System.out.print(array[i] + " ");
    }
    return;
  }
  
  /*The following method was developed in class*/
  public static void binarySearch(int targetGrade, int[] grades) {
    int low = 0;
    int high = grades.length-1;
    int iterations = 0;
    while(high >= low) {
      iterations++;
      int mid = (low + high)/2;
      if (targetGrade < grades[mid]) {
        high = mid - 1;
      }
    else if (targetGrade == grades[mid]) {
      System.out.println(targetGrade + " was found at " + mid + " after " + iterations + " iterations.");
      return;
      }
    else {
      low = mid + 1;
    }
  }
  System.out.println(targetGrade + " was not found after " + iterations + " iterations.");
  return;
  }
  
  /*The following method is modified from my work in hw08*/
  public static void scramble(int[] grades) {
    Random random = new Random(); //Declares a random generator
    int holder; //A holder variable to swap values with
    int randomSpot; //An int to represent what card value to switch
    for (int i = 0; i < 100; i++) { //The loop runs 100 times, switching a random card with the first card
      randomSpot = random.nextInt(15); //New random number to represent a card
      holder = grades[randomSpot]; //Next three lines are switching the card at 0 and the random number
      grades[randomSpot] = grades[0];
      grades[0] = holder;
    }
    return;
  }
  
  public static void linearSearch(int targetGrade, int[] grades) {
    for (int i = 0; i < 15; i++) { //Runs for each value of the loop
      if (grades[i] == targetGrade) { //Enters if target grade was found
        System.out.println(targetGrade + " was found after " + i + " iterations.");
        return; //Exits once found
      }
    }
    System.out.println(targetGrade + " was not found in the list after 15 iterations.");
    return; //Exits if not found
  }
} 